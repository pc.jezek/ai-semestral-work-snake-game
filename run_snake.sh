#!/bin/bash

function print_requirements () {
    echo 'Please install following packages using "pip install" or "conda install":'
    echo "tkinter"
    echo "numpy"
}

python -c '
try:
    import tkinter
    import numpy
    import enum
except ModuleNotFoundError as e:
    print("Error:", e.name, "not found.")
    exit(1)' || (print_requirements && exit)

python snake $@

