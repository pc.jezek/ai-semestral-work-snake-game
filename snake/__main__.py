from argparse import ArgumentParser
from application import Application

parser = ArgumentParser()
parser.add_argument('--save', action='store', dest='save_file', metavar='File', help='save trained neural net to file')
parser.add_argument('--open', action='store', dest='open_file', metavar='File', help='open trained neural net')
parser.add_argument('-p', action='store_true', dest='p', help='human player mode')
parser.add_argument('--height', action='store', dest='h', help='board height (default 10)', type=int)
parser.add_argument('--width', action='store', dest='w', help='board width (default 10)', type=int)
parser.add_argument('--speed', action='store', dest='speed', help='game speed (default 10)', type=int)
parser.add_argument('--gen', action='store', dest='generations', help='number of generations (default 50)', type=int)
parser.add_argument('--population-size', action='store', dest='size',
                    help='number of individuals in population (default 50)', type=int)
# parser.add_argument("substring", help="podretezec")
# parser.add_argument("vystup", help="vystup")
args = parser.parse_args()

if __name__ == '__main__':
    WIDTH = args.w if args.w else 10
    HEIGHT = args.h if args.h else 10
    SIZE = args.size if args.size else 50
    GENERATIONS = args.generations if args.generations else 50
    SPEED = args.speed if args.speed else 10

    application = Application(HEIGHT, WIDTH, SIZE, GENERATIONS, SPEED)

    if args.save_file:
        application.train()
        application.save(args.save_file)
    elif args.open_file:
        application.open(args.open_file)
    elif args.p:
        application.run(player=True)
        exit(0)
    else:
        application.train()

    application.run()
