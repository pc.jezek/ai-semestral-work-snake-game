from game import Game
from game_view import PlayerView, SimulationView
from neural_net.snake_neural_network import GeneticAlgorithm, SnakeNN, Individual


class Application:
    def __init__(self, rows, cols, pop_size, generations, speed):
        self.rows = rows
        self.cols = cols
        self.pop_size = pop_size
        self.generations = generations
        self.speed = speed
        self.trained_individual = None

    def train(self):
        """
        Train the neural network and store best individual in the last generation
        """
        ga = GeneticAlgorithm(self.pop_size, self.generations, self.rows, self.cols)
        self.trained_individual = ga.train()
        return self.trained_individual

    def run(self, player=False):
        """
        Run the game with the trained individual or with human player
        :param player: whether the player is human or not
        """
        view = SimulationView(SnakeNN(self.trained_individual), self.rows, self.cols, self.speed) if not player \
            else PlayerView(self.rows, self.cols, self.speed)

        simulation_game = Game(self.rows, self.cols, view)
        simulation_game.run()

    def open(self, file):
        """
        Open trained individual from file
        :param file: filename of json with genotype
        """
        self.trained_individual = Individual(self.rows, self.cols, file=file)

    def save(self, file):
        """
        Save individual to file
        :param file: filename where json with genotype will be saved
        """
        self.trained_individual.save(file)
