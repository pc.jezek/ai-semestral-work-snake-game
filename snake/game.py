from abc import ABC, abstractmethod
from enum import Enum
import numpy as np

from typing import List

from game_view import View

SNAKE_COLOR = '#FFFFFF'
EMPTY_COLOR = '#130f40'
FOOD_COLOR = '#D81B60'


def singleton(class_):
    inst = None

    def instance(*args, **kwargs):
        nonlocal inst
        if not inst:
            inst = class_(*args, **kwargs)
        return inst

    return instance


class GameObject(ABC):
    """
    Abstract class for game objects
    """

    def canStep(self):
        return True

    @abstractmethod
    def draw(self, view, row, col):
        raise NotImplementedError


@singleton
class SnakeTile(GameObject):

    def canStep(self):
        return False

    def draw(self, view, row, col):
        view.add_rectangle(row, col, SNAKE_COLOR)


@singleton
class FoodTile(GameObject):
    def draw(self, view, row, col):
        view.add_rectangle(row, col, EMPTY_COLOR)
        view.add_circle(row, col, FOOD_COLOR)


@singleton
class EmptyTile(GameObject):
    def draw(self, view, row, col):
        view.add_rectangle(row, col, EMPTY_COLOR)


class DIRECTION(Enum):
    DOWN = 0
    UP = 1
    LEFT = 2
    RIGHT = 3

    @staticmethod
    def point_on_left(direction, ref_point, point1):
        if direction == DIRECTION.DOWN:
            return point1.col > ref_point.col
        elif direction == DIRECTION.UP:
            return point1.col < ref_point.col
        elif direction == DIRECTION.LEFT:
            return point1.row > ref_point.row
        elif direction == DIRECTION.RIGHT:
            return point1.row < ref_point.row

    @staticmethod
    def point_on_right(direction, ref_point, point1):
        if direction == DIRECTION.DOWN:
            return point1.col < ref_point.col
        elif direction == DIRECTION.UP:
            return point1.col > ref_point.col
        elif direction == DIRECTION.LEFT:
            return point1.row < ref_point.row
        elif direction == DIRECTION.RIGHT:
            return point1.row > ref_point.row

    @staticmethod
    def point_ahead(direction, ref_point, point1):
        if direction == DIRECTION.DOWN:
            return ref_point.row < point1.row
        elif direction == DIRECTION.UP:
            return ref_point.row > point1.row
        elif direction == DIRECTION.LEFT:
            return ref_point.col > point1.col
        elif direction == DIRECTION.RIGHT:
            return ref_point.col < point1.col

    @staticmethod
    def vector(direction):
        if direction == DIRECTION.DOWN:
            return 1, 0
        elif direction == DIRECTION.UP:
            return -1, 0
        elif direction == DIRECTION.LEFT:
            return 0, -1
        elif direction == DIRECTION.RIGHT:
            return 0, 1

    @staticmethod
    def keysym(direction):
        if direction == DIRECTION.DOWN:
            return 'Down'
        elif direction == DIRECTION.UP:
            return 'Up'
        elif direction == DIRECTION.LEFT:
            return 'Left'
        elif direction == DIRECTION.RIGHT:
            return 'Right'

    @staticmethod
    def opposite(direction):
        if direction == DIRECTION.DOWN:
            return DIRECTION.UP
        elif direction == DIRECTION.UP:
            return DIRECTION.DOWN
        elif direction == DIRECTION.LEFT:
            return DIRECTION.RIGHT
        elif direction == DIRECTION.RIGHT:
            return DIRECTION.LEFT

    @staticmethod
    def left_of(direction):
        if direction == DIRECTION.DOWN:
            return DIRECTION.RIGHT
        elif direction == DIRECTION.UP:
            return DIRECTION.LEFT
        elif direction == DIRECTION.LEFT:
            return DIRECTION.DOWN
        elif direction == DIRECTION.RIGHT:
            return DIRECTION.UP

    @staticmethod
    def right_of(direction):
        return DIRECTION.opposite(DIRECTION.left_of(direction))


class Coords:
    """
    Row, col coordinates
    """
    def __init__(self, row=0, col=0):
        self.row = row
        self.col = col

    def __str__(self):
        return f'({self.row}, {self.col})'

    def __eq__(self, other):
        return self.row == other.row and self.col == other.col

    @staticmethod
    def dist(x, y):
        return np.hypot((x.row - y.row), (x.col - y.col))


class Snake:
    def _create_snake(self) -> List[Coords]:
        head = Coords(self.rows // 2, self.cols // 2)
        return [Coords(head.row, head.col),
                Coords(head.row - 1, head.col),
                Coords(head.row - 2, head.col)]

    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self._tile_coords = self._create_snake()

    @property
    def tile_coords(self):
        return self._tile_coords

    def can_move(self, direction: DIRECTION):
        """
        Check if the snake can move in a given direction
        :param direction: where should the snake move
        :return: False if the snake would collide
        """
        new_head = None
        head_row, head_col = self._tile_coords[0].row, self._tile_coords[0].col
        if direction == DIRECTION.DOWN:
            new_head = Coords(head_row + 1, head_col)
        elif direction == DIRECTION.UP:
            new_head = Coords(head_row - 1, head_col)
        elif direction == DIRECTION.RIGHT:
            new_head = Coords(head_row, head_col + 1)
        elif direction == DIRECTION.LEFT:
            new_head = Coords(head_row, head_col - 1)

        if not new_head:
            return None

        head_row, head_col = new_head.row, new_head.col
        if (head_row < 0 or head_row >= self.rows
                or head_col < 0 or head_col >= self.cols
                or self._tile_coords[0] in self._tile_coords[1:]):
            return None
        return new_head

    def move(self, direction: DIRECTION, food_location: Coords) -> bool:
        """
        Move the snake if it can move,
        if food was consumed, make the snake longer by 1 block
        :param direction: where should the snake move next
        :param food_location: location of the food
        :return: False if the snake collided
        """
        next_head = self.can_move(direction)
        if not next_head:
            return False

        self._tile_coords.insert(0, next_head)

        if not self._tile_coords[0] == food_location:
            del self._tile_coords[-1]

        return True


class Game:
    def __init__(self, rows, cols, view: View = None, seed=None):
        self.r = np.random.RandomState()
        if seed is not None:
            self.r.seed(seed)
        self.rows = rows
        self.cols = cols
        self.score = 0
        self.food = self.snake = self.direction = self.next_direction = None
        self.view = view
        if view:
            view.register_update_handler(self.handle_frame)
            view.register_keypress_handler(self.handle_keypress)
            view.register_game_over_handler(self._initialize)
        self.board: List[List[GameObject]] = []
        self._initialize()

    def _initialize(self):
        """
        Initialization and game over handler
        Reset the game
        """
        self.score = 0
        self._clear_board()
        self.snake = Snake(self.rows, self.cols)
        self._set_random_food()
        self.direction = DIRECTION.DOWN
        self.next_direction = DIRECTION.DOWN
        for c in self.snake.tile_coords:
            self.board[c.row][c.col] = SnakeTile()

    def handle_keypress(self, key):
        """
        Keypress handler,
        set next direction according to the key pressed
        :param key: key pressed
        """
        direction = None
        if key == 'Left':
            direction = DIRECTION.LEFT
        elif key == 'Right':
            direction = DIRECTION.RIGHT
        elif key == 'Down':
            direction = DIRECTION.DOWN
        elif key == 'Up':
            direction = DIRECTION.UP
        else:
            return
        if self.direction == DIRECTION.opposite(direction):
            return
        self.next_direction = direction

    def _clear_board(self):
        self.board = [[EmptyTile()] * self.cols for _ in range(self.rows)]

    def _update_board(self, ):
        self._clear_board()
        for c in self.snake.tile_coords:
            self.board[c.row][c.col] = SnakeTile()
        self.board[self.food.row][self.food.col] = FoodTile()

    def wall_and_snake_dists(self):
        """
        Compute distances for input layer of the neural network
        """
        s_head = self.snake.tile_coords[0]
        w = dict()
        s = dict()

        w[DIRECTION.UP] = s_head.row / self.rows
        w[DIRECTION.DOWN] = (self.rows - s_head.row - 1) / self.rows
        w[DIRECTION.LEFT] = s_head.col / self.cols
        w[DIRECTION.RIGHT] = (self.cols - s_head.col - 1) / self.cols

        s[DIRECTION.UP] = w[DIRECTION.UP]
        s[DIRECTION.DOWN] = w[DIRECTION.DOWN]
        s[DIRECTION.LEFT] = w[DIRECTION.LEFT]
        s[DIRECTION.RIGHT] = w[DIRECTION.RIGHT]

        i = 1
        while s_head.row - i >= 0:
            if not self.board[s_head.row - i][s_head.col].canStep():
                s[DIRECTION.UP] = i / self.rows
                break
            i += 1

        i = 1
        while s_head.row + i < self.rows:
            if not self.board[s_head.row + i][s_head.col].canStep():
                s[DIRECTION.DOWN] = i / self.rows
                break
            i += 1

        i = 1
        while s_head.col - i >= 0:
            if not self.board[s_head.row][s_head.col - i].canStep():
                s[DIRECTION.LEFT] = i / self.cols
                break
            i += 1

        i = 1
        while s_head.col + i < self.cols:
            if not self.board[s_head.row][s_head.col + i].canStep():
                s[DIRECTION.RIGHT] = i / self.cols
                break
            i += 1
        return ((w[DIRECTION.left_of(self.direction)], w[self.direction], w[DIRECTION.right_of(self.direction)]),
                (s[DIRECTION.left_of(self.direction)], s[self.direction], s[DIRECTION.right_of(self.direction)]))

    def handle_frame(self):
        """
        Update handler, handle one step of the game,
        move the snake in the direction that was set by last keypress
        :return:
        """
        self.direction = self.next_direction
        if not self.snake.move(self.direction, self.food):
            return None, self.score, self
        if self.snake.tile_coords[0] == self.food:
            self.score += 1
            self._set_random_food()
        self._update_board()
        return self.board, self.score, self

    def _set_random_food(self):
        """
        Find random position for the new food
        """
        choices = []
        for row in range(self.rows):
            for col in range(self.cols):
                if Coords(row, col) not in self.snake.tile_coords:
                    choices.append(Coords(row, col))
        self.food = self.r.choice(choices)

    def run(self):
        """
        Run the game
        """
        if self.view:
            self.view.run()
        else:
            raise ValueError('No view supplied!')
