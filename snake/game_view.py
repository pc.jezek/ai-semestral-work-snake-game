import tkinter as tk
from abc import ABC, abstractmethod

TILE_SIZE = 50
FRAME_MS = 1400


class View(ABC):
    """
    Abstract view
    Handlers registration is implemented
    """
    def __init__(self, rows: int, cols: int):
        self.update_handler = None
        self.keypress_handler = None
        self.game_over_handler = None
        self.is_game_over = False
        self.current_board = []
        self.rows = rows
        self.cols = cols

    @abstractmethod
    def run(self):
        raise NotImplementedError

    def register_update_handler(self, handler: ()):
        self.update_handler = handler

    def register_keypress_handler(self, handler: ()):
        self.keypress_handler = handler

    def register_game_over_handler(self, handler: ()):
        self.game_over_handler = handler


class GameView(View):
    """
    GUI canvas View
    """
    def __init__(self, rows: int, cols: int, speed: int):
        super().__init__(rows, cols)
        self.height = rows * TILE_SIZE
        self.width = cols * TILE_SIZE
        self.speed = FRAME_MS // speed

        self.window = tk.Tk()
        self.window.title('Snake')
        self.window.resizable(False, False)
        self.window.geometry(f'{self.width}x{self.height + 40}')
        self.window.configure(background='#000000')

        self.canvas = tk.Canvas(self.window,
                                width=self.width,
                                height=self.height,
                                background="#130f40")
        self.canvas.pack()

        self.score = tk.Label(self.window, text='Score: 0', foreground='#ffffff',
                              height=40,
                              font=('Courier', 18),
                              background='#000000')
        self.score.pack()

    def run(self):
        self._update()
        self.window.mainloop()

    @abstractmethod
    def _update(self):
        raise NotImplementedError

    def _game_over(self, score):
        self.canvas.create_rectangle(0, self.height // 5,
                                     self.width, self.height - self.height // 5, fill='#000000')
        self.canvas.create_text(self.width // 2, self.height // 2 - 30,
                                fill='#ffffff', text='GAME OVER',
                                font=('Courier', 18, 'bold'))
        self.canvas.create_text(self.width // 2, self.height // 2,
                                fill='#ffffff', text='SCORE: ' + str(score),
                                font=('Courier', 18))
        self.canvas.create_text(self.width // 2, self.height // 2 + 30,
                                fill='#ffffff', text='Press <ENTER> to start again',
                                font=('Courier', 16))

    def add_rectangle(self, row, col, color='#000000'):
        x_px = col * TILE_SIZE
        y_px = row * TILE_SIZE
        self.canvas.create_rectangle(x_px, y_px, x_px + TILE_SIZE, y_px + TILE_SIZE, fill=color)

    def add_circle(self, x, y, color='#000000'):
        o = TILE_SIZE // 6
        x_px = y * TILE_SIZE + o
        y_px = x * TILE_SIZE + o
        self.canvas.create_oval(x_px, y_px,
                                x_px + TILE_SIZE - 2 * o, y_px + TILE_SIZE - 2 * o,
                                fill=color)


class PlayerView(GameView):
    """
    Human player view
    """

    def __init__(self, *args):
        super().__init__(*args)
        self.window.bind('<Key>', self._key_pressed)

    def _key_pressed(self, event):
        if self.is_game_over and event.keysym == 'Return':
            self.game_over_handler()
            self.current_board = None
            self.is_game_over = False
            self._update()

        self.keypress_handler(event.keysym)

    def _update(self):
        next_board, next_score, _ = self.update_handler()
        if next_board is None:
            self.is_game_over = True
            self._game_over(next_score)
            return

        for row in range(len(next_board)):
            for col in range(len(next_board[0])):
                if self.current_board and self.current_board[row][col] == next_board[row][col]:
                    continue
                next_board[row][col].draw(self, row, col)
        self.current_board = next_board
        self.score.configure(text='SCORE: ' + str(next_score))
        self.window.after(self.speed, self._update)


class SimulationView(GameView):
    """
    AI player view
    """
    def __init__(self, nn, rows, cols, speed):
        super().__init__(rows, cols, speed)
        self.nn = nn
        self.window.bind('<Key>', self._key_pressed)

    def _update(self):
        next_board, next_score, game = self.update_handler()
        if next_board is None:
            self.is_game_over = True
            self._game_over(next_score)
            return

        for row in range(len(next_board)):
            for col in range(len(next_board[0])):
                if self.current_board and self.current_board[row][col] == next_board[row][col]:
                    continue
                next_board[row][col].draw(self, row, col)
        self.current_board = next_board
        self.score.configure(text='SCORE: ' + str(next_score))
        self.window.after(self.speed, self._update)
        keycode = self.nn.feed_forward(game)

        self.keypress_handler(keycode)

    def _game_over(self, score):
        super(SimulationView, self)._game_over(score)
        self.canvas.create_text(self.width // 2, self.height // 2 + 30,
                                fill='#ffffff', text='Press <ENTER> to start again',
                                font=('Courier', 16))

    def _key_pressed(self, event):
        if event.keysym == 'Return':
            self.game_over_handler()
            self.current_board = None
            if self.is_game_over:
                self._update()
            self.is_game_over = False
