import json

import numpy as np

from game import Game, DIRECTION, Coords

I_SIZE = 10
H1_SIZE = 18
H2_SIZE = 9
O_SIZE = 3


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


class SnakeNN:
    def __init__(self, individual: 'Individual'):
        self.individual = individual
        self._decode_weights(individual)

    def _decode_weights(self, individual):
        """
        Slice 1D vector of real numbers into weights of neural network
        :param individual: genotype
        """
        self.w_input = individual.genotype[0:I_SIZE * H1_SIZE].reshape((H1_SIZE, I_SIZE))
        offset = I_SIZE * H1_SIZE
        self.w_h1 = individual.genotype[offset:offset + H1_SIZE * H2_SIZE].reshape((H2_SIZE, H1_SIZE))
        offset += H1_SIZE * H2_SIZE
        self.w_h2 = individual.genotype[offset:offset + H2_SIZE * O_SIZE].reshape((O_SIZE, H2_SIZE))

    def _get_input_layer(self, game):
        """
        Extract input layer values from game state
        :param game: Current state of the game
        """
        input_l = []
        snake_head = game.snake.tile_coords[0]

        direction = game.direction

        dists = game.wall_and_snake_dists()
        # Distances to walls
        input_l.extend(dists[0])
        # Distances to snake
        input_l.extend(dists[1])

        # Fruit is on the left
        input_l.append(float(DIRECTION.point_on_left(direction, snake_head, game.food)))

        # Fruit is on the right
        input_l.append(float(DIRECTION.point_on_right(direction, snake_head, game.food)))

        # Fruit is ahead
        input_l.append(float(DIRECTION.point_ahead(direction, snake_head, game.food)))

        # Distance to fruit
        input_l.append(Coords.dist(game.snake.tile_coords[0], game.food) / np.hypot(game.rows, game.cols))
        return input_l

    def feed_forward(self, game):
        """
        Feed input forward through the neural network
        :param game: Current state of the game
        :return: Key that should be pressed next
        """
        input_l = self._get_input_layer(game)

        h1 = [sigmoid(np.dot(input_l, self.w_input[i])) for i in range(H1_SIZE)]
        h2 = [sigmoid(np.dot(h1, self.w_h1[i])) for i in range(H2_SIZE)]
        out = [sigmoid(np.dot(h2, self.w_h2[i])) for i in range(O_SIZE)]

        best = np.argmax(out)
        if best == 0:
            return DIRECTION.keysym(DIRECTION.left_of(game.direction))
        elif best == 1:
            return DIRECTION.keysym(DIRECTION.right_of(game.direction))
        else:
            return DIRECTION.keysym(game.direction)


class Individual:
    """
    Class representing the genotype
    """

    def __init__(self, rows, cols, genotype=None, file=None):
        """
        If no genotype is provided, generate random one,
        otherwise use the copy of provided genotype
        """
        self.rows = rows
        self.cols = cols
        self.fitness = 0
        if file is not None:
            self.open(file)
            return

        if genotype is None:
            self._generate_random_individual()
        else:
            self.genotype = genotype.copy()

    def copy(self):
        return Individual(self.rows, self.cols, self.genotype)

    def _generate_random_individual(self):
        """
        Generate random genotype from standard normal distribution [-1,1]
        """
        self.genotype = np.clip(np.random.standard_normal(size=I_SIZE * H1_SIZE + H1_SIZE * H2_SIZE + H2_SIZE * O_SIZE),
                                -1, 1)

    def mutation(self, probability: float):
        """
        Mutate each weight in genotype by offset from
        normal distribution with dispersion 0.5 with
        probability given by parameter
        :param probability: Each weight mutation probability
        """
        for i in range(len(self.genotype)):
            if np.random.random() < probability:
                self.genotype[i] += np.random.normal(scale=0.5)

    def calculate_fitness(self):
        """
        Simulate the game two times and set fitness based on the
        average of squares (score + 1)^2
        """
        nn = SnakeNN(self)
        for i in range(2):
            game = Game(self.rows, self.cols, seed=(i + 42) * 666)
            next_board, *_ = game.handle_frame()
            steps = 0
            while next_board is not None and steps < 600:
                steps += 1
                keypress = nn.feed_forward(game)
                game.handle_keypress(keypress)
                next_board, *_ = game.handle_frame()
            tmpfitness = (game.score + 1) ** 2
            self.fitness += tmpfitness
        self.fitness /= 2

    def save(self, filename):
        """
        Save weights in json to file given by parameter
        """
        with open(filename, 'w', encoding='utf-8') as f:
            json.dump(self.genotype.tolist(), f)

    def open(self, path):
        """
        Load weights from file given by parameter
        """
        with open(path, encoding='utf-8') as f:
            self.genotype = np.array(json.load(f))


class GeneticAlgorithm:
    def __init__(self, population_size, generations, rows, cols):
        self.population_size = population_size
        self.generations = generations
        self.rows = rows
        self.cols = cols
        self.population = [Individual(rows, cols) for _ in range(population_size)]

    def evolve(self):
        """
        Evolve one generation (fitness calculation, selection, mutation)
        """
        self.calculate_fitness()
        best_fit = self.population[0]
        sum_fit = 0
        for x in self.population:
            sum_fit += x.fitness
            if x.fitness > best_fit.fitness:
                best_fit = x

        print(f'avg fit: {sum_fit / self.population_size} best fit: {best_fit.fitness}')
        sort_p = sorted(self.population, key=lambda p: p.fitness, reverse=True)
        print('top 5:', [x.fitness for x in sort_p[:5]])

        self.population = self.select_individuals(self.population_size)

        for individual in self.population:
            individual.mutation(.03)
        return best_fit

    def train(self):
        """
        Train the neural network with genetic algorithm
        :return: best individual from the last generation
        """
        for i in range(self.generations):
            print(i, end=': ')
            best_fit = self.evolve()

        return SnakeNN(best_fit).individual

    def select_individuals(self, count):
        """
        Roulette wheel selection of individuals
        :param count: how many should be selected
        :return: selected individuals
        """
        selected = []

        total_sum = sum([x.fitness for x in self.population])
        while not len(selected) == count:
            choice = np.random.uniform(0, total_sum)
            tmp_sum = 0
            for x in self.population:
                tmp_sum += x.fitness
                if choice <= tmp_sum:
                    selected.append(x.copy())
                    break

        return selected

    def calculate_fitness(self):
        """
        Calculate fitness of the whole population
        """
        for individual in self.population:
            individual.calculate_fitness()
